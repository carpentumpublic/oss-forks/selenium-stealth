# Fork changelog

## v1.0.8

* added py.typed marker

## v1.0.7

* Update JS files to [puppeteer-extra-plugin-stealth@2.11.1](https://github.com/berstend/puppeteer-extra/tree/puppeteer-extra-plugin-stealth%402.11.1/packages/puppeteer-extra-plugin-stealth/evasions)
* Utils are now part of each evasion function. The previous way did not work for me.
