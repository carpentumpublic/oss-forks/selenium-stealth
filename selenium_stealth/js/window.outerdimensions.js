// Taken from https://github.com/berstend/puppeteer-extra/tree/master/packages/puppeteer-extra-plugin-stealth/evasions

/**
 * Fix missing window.outerWidth/window.outerHeight in headless mode
 * Will also set the viewport to match window size, unless specified by user
 */
() => {
  //HERE_PLACE_UTILS

  try {
    if (window.outerWidth && window.outerHeight) {
      return // nothing to do here
    }
    const windowFrame = 85 // probably OS and WM dependent
    window.outerWidth = window.innerWidth
    window.outerHeight = window.innerHeight + windowFrame
  } catch (err) {}
}
