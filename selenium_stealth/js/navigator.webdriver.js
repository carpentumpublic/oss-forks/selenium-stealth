// Taken from https://github.com/berstend/puppeteer-extra/tree/master/packages/puppeteer-extra-plugin-stealth/evasions

/**
 * Pass the Webdriver Test.
 * Will delete `navigator.webdriver` property.
 */
() => {
  //HERE_PLACE_UTILS

  if (navigator.webdriver === false) {
    // Post Chrome 89.0.4339.0 and already good
  } else if (navigator.webdriver === undefined) {
    // Pre Chrome 89.0.4339.0 and already good
  } else {
    // Pre Chrome 88.0.4291.0 and needs patching
  delete Object.getPrototypeOf(navigator).webdriver
  }
}
