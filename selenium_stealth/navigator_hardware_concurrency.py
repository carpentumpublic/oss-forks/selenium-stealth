from pathlib import Path

from selenium.webdriver import Chrome as Driver

from .wrapper import evaluateOnNewDocument


def navigator_hardware_concurrency(driver: Driver, hardwareConcurrency: int, **kwargs) -> None:
  evaluateOnNewDocument(
    driver, Path(__file__).parent.joinpath("js/navigator.hardwareConcurrency.js").read_text(),
    hardwareConcurrency
  )
